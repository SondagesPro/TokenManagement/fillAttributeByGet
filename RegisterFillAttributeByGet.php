<?php

/**
 * Fill the attribute via GET request when register
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2016-2023 ValueMatch <http://valuematch.net>
 * @copyright 2016-2023 Denis Chenu <http://www.sondages.pro>
 * @license GPL v3
 * @version 0.1.1
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

class RegisterFillAttributeByGet extends PluginBase
{
    protected $storage = 'DbStorage';
    protected static $description = 'Allow prefilling attribute value by GET parameters with registration.';
    protected static $name = 'RegisterFillAttributeByGet';

    public function init()
    {
        $this->subscribe('beforeRegister', 'addGetValue');
    }

    public function addGetValue()
    {
        $beforeRegister = $this->getEvent();
        $surveyId = $beforeRegister->get("surveyid");
        if (!Yii::app()->request->getIsPostRequest()) {
            $aSurveyAttributes = Survey::model()->findByPk($surveyId)->getTokenAttributes();
            $GetParams = array();
            foreach ($aSurveyAttributes as $field => $attribute) {
                if (empty($attribute['show_register']) || $attribute['show_register'] != 'Y') {
                    if (App()->request->getQuery($attribute['description'])) {
                        $GetParams[$field] = strval(App()->request->getQuery($attribute['description']));
                    }
                }
            }
            if (!empty($GetParams)) {
                App()->user->setState("fillAttributeByGet_GetParams_{$surveyId}", $GetParams);
            }
        }
        /* Register only if need */
        if (App()->user->getState("fillAttributeByGet_GetParams_{$surveyId}")) {
            $this->subscribe('beforeTokenSave');
        }
    }

    /**
     * Set the attribute according to state value
     * We are surely in register part of event
     */
    public function beforeTokenSave()
    {
        $beforeTokenSave = $this->getEvent();
        $surveyId = $beforeTokenSave->get("surveyId");
        if (App()->user->getState("fillAttributeByGet_GetParams_{$surveyId}")) {
            $GetParams = App()->user->getState("fillAttributeByGet_GetParams_{$surveyId}");
            $oToken = $beforeTokenSave->get("model");
            $aSurveyAttributes = Survey::model()->findByPk($surveyId)->getTokenAttributes();
            foreach ($aSurveyAttributes as $field => $attribute) {
                $value = $GetParams[$field];
                if ($attribute['encrypted'] == 'Y') {
                    $value = LSActiveRecord::encryptSingle($value);
                }
                if (isset($GetParams[$field])) {
                    $oToken->setAttribute($field, $value);
                }
            }
        }
        /* reset state */
        App()->user->setState("fillAttributeByGet_GetParams_{$surveyId}", null);
    }
}

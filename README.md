# RegisterFillAttributeByGet

Get the $_GET parameters first time user come to register page. Use this parameters to fill the token attribute value for attribute not in register page.

## Documentation

Prefilling is done by attribute description, for example if you need to prefill with a parameter like `clientid`, add an attribute with description clientid and don't show it in register page.

## Copyright and homepage
- Copyright © 2016-2023 ValueMatch <http://valuematch.net>
- Copyright © 2016-2023 Denis Chenu <http://sondages.pro>
- Licence : GNU General Public License <https://www.gnu.org/licenses/gpl-3.0.html>
- [Professional support](http://support.sondages.pro/)
- [Issues and contibute](https://gitlab.com/SondagesPro/TokenManagement/RegisterFillAttributeByGet/-/issues)
- [Donate](https://support.sondages.pro/open.php?topicId=12), [Liberapay](https://liberapay.com/SondagesPro/), [OpenCollective](https://opencollective.com/sondagespro) 
